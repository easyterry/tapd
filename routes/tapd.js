/* eslint-disable */

const router = require('koa-router')()
const userModel = require('./config')

router.prefix('/tapd')

router.post('/getAliveData', async (ctx) => {
	await userModel.findAliveData().then((res) => {
		if (res.length) {
			ctx.body = {
				code: 0,
				data: res,
				msg: 'get users data successful',
			}
		} else {
			ctx.body = {
				code: 1,
				data: [],
				msg: 'data not found',
			}
		}
	})
})

router.post('/getStatusDuration', async (ctx) => {
	await userModel.getStatusDuration(ctx.request.body.IDList).then(res => {
		if (res.length) {
			ctx.body = {
				code: 0,
				data: res,
				msg: 'done',
			}
		} else {
			ctx.body = {
				code: 1,
				data: [],
				msg: 'data not found',
			}
		}
	})
})

module.exports = router
